#include <cassert>

#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <limits>
#include "tgaimage.h"
#include "model.h"
#include "geometry.h"

#include <cmath>
#include <utility>
#include <iostream>

const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red   = TGAColor(255, 0,   0,   255);
const TGAColor green   = TGAColor(0, 255,   0,   255);
const TGAColor blue   = TGAColor(0, 0,   255,   255);

Model *model = nullptr;
const int width = 800;
const int height = 800;

int getZ(Vec3i p0, Vec3i p1, Vec2i p)
{
    // p0, p1 and p are on the same line, returns p's z value based on its x and y

    int delta_z1 = p1.z - p0.z;

    if (p1.x - p0.x != 0)
    {
        int delta_x = p.x - p0.x;
        int delta_x1 = p1.x - p0.x;

        return (delta_x * delta_z1 / delta_x1) + p0.z;
    }
    else if (p1.y - p0.y != 0)
    {
        int delta_y = p.y - p0.y;
        int delta_y1 = p1.y - p0.y;

        return (delta_y * delta_z1 / delta_y1) + p0.z;
    }
    else
        return std::numeric_limits<int>::min();
}

int getX(Vec2i p0, Vec2i p1, int y)
{
    int delta_y = y - p0.y;
    int delta_y1 = p1.y - p0.y;
    int delta_x1 = p1.x - p0.x;

    if (delta_y1 != 0)
        return (delta_x1 * delta_y / delta_y1) + p0.x;
    else
        return p0.x;
}

void line(Vec2i p0, Vec2i p1, TGAImage &image, TGAColor col)
{
    int x0 = p0.x;
    int y0 = p0.y;

    int x1 = p1.x;
    int y1 = p1.y;

    bool steep = false;
    if (std::abs(x1 - x0) < std::abs(y1 - y0))
    {
        std::swap(x0, y0);
        std::swap(x1, y1);

        steep = true;
    }

    if (x0 > x1)
    {
        std::swap(x0, x1);
        std::swap(y0, y1);
    }

    const int dx = x1 - x0;
    const int dy = y1 - y0;
    const float derror = std::abs(dy / (float)dx);
    float error = 0.;

    int y = y0;
    for (int x = x0; x <= x1; x++)
    {
        if (steep)
            image.set(y, x, col);
        else
            image.set(x, y, col);

        error += derror;

        if (error > .5)
        {
            y += (y1 > y0 ? 1 : -1);
            error -= 1.;
        }
    }
}

void horizontal_line_3d(Vec3i p0, Vec3i p1, Vec2i uvleft, Vec2i uvright, TGAImage &image, float intensity, int zbuffer[])
{
    assert(p0.y == p1.y);
    assert(uvleft.y == uvright.y);

    if (p0.x > p1.x)
        std::swap(p0, p1);
    if (uvleft.x > uvright.x)
        std::swap(uvleft, uvright);

    int length = p1.x - p0.x;
    if (length == 0)
        return;
    int uvlength = uvright.x - uvleft.x;

    for (int x = p0.x; x <= p1.x; x++)
    {
        int delta_x = x - p0.x;
        int delta_uvx = delta_x * uvlength / length;
        int uvx = uvleft.x + delta_uvx;

        TGAColor color = model->diffuse(Vec2i(uvx, uvleft.y));

        int z = getZ(p0, p1, Vec2i(x, p0.y));
        int idx = x + p0.y * width;
        if (zbuffer[idx] < z)
        {
            zbuffer[idx] = z;
            image.set(x, p0.y, TGAColor(int(color.r * intensity), int(color.g * intensity), int(color.b*intensity), 255));
        }
    }
}

void fill_bottom_flat_triangle(Vec3i p0, Vec3i p1, Vec3i p2, Vec2i uv0, Vec2i uv1, Vec2i uv2, TGAImage &image, float intensity, int zbuffer[])
{
    // p1 and p2 are on the same line, p0 is above that line
    assert(p1.y == p2.y);
    assert(p0.y >= p1.y);
    assert(uv1.y == uv2.y);
    assert(uv0.y >= uv1.y);

    float invslope1 = (p1.x - p0.x) / (float)(p1.y - p0.y);
    float invslope2 = (p2.x - p0.x) / (float)(p2.y - p0.y);

    int height = p0.y - p1.y;
    int uvheight = uv0.y - uv1.y;

    float xleft = p0.x;
    float xright = p0.x;

    for (int scanlineY = p0.y; scanlineY >= p1.y; scanlineY--)
    {
        int scanline_height = p0.y - scanlineY;
        int uv_scanline_height = 0;
        if (height != 0)
            uv_scanline_height = uvheight * scanline_height / height;
        int uv_scanline_y = uv0.y - uv_scanline_height;

        int uvxleft = getX(uv0, uv1, uv_scanline_y);
        int uvxright = getX(uv0, uv2, uv_scanline_y);

        Vec2i uvleft(uvxleft, uv_scanline_y);
        Vec2i uvright(uvxright, uv_scanline_y);

        int zleft = getZ(p0, p1, Vec2i(xleft, scanlineY));
        int zright = getZ(p0, p2, Vec2i(xright, scanlineY));

        Vec3i t0(int(xleft), scanlineY, zleft);
        Vec3i t1(int(xright), scanlineY, zright);

        horizontal_line_3d(t0, t1, uvleft, uvright, image, intensity, zbuffer);

        xleft -= invslope1;
        xright -= invslope2;
    }
}

void fill_top_flat_triangle(Vec3i p0, Vec3i p1, Vec3i p2, Vec2i uv0, Vec2i uv1, Vec2i uv2, TGAImage &image, float intensity, int zbuffer[])
{
    // p1 and p2 are on the same line, p0 is below that line
    assert(p1.y == p2.y);
    assert(p0.y <= p1.y);
    assert(uv1.y == uv2.y);
    assert(uv0.y <= uv1.y);

    float invslope1 = (p1.x - p0.x) / (float)(p1.y - p0.y);
    float invslope2 = (p2.x - p0.x) / (float)(p2.y - p0.y);

    int height = p1.y - p0.y;
    int uvheight = uv1.y - uv0.y;

    float xleft = p0.x;
    float xright = p0.x;

    for (int scanlineY = p0.y; scanlineY <= p1.y; scanlineY++)
    {
        int scanline_height = scanlineY - p0.y;
        int uv_scanline_height = 0;
        if (height != 0)
            uv_scanline_height = uvheight * scanline_height / height;
        int uv_scanline_y = uv0.y + uv_scanline_height;

        int uvxleft = getX(uv0, uv1, uv_scanline_y);
        int uvxright = getX(uv0, uv2, uv_scanline_y);

        Vec2i uvleft(uvxleft, uv_scanline_y);
        Vec2i uvright(uvxright, uv_scanline_y);

        int zleft = getZ(p0, p1, Vec2i(xleft, scanlineY));
        int zright = getZ(p0, p2, Vec2i(xright, scanlineY));

        Vec3i t0(int(xleft), scanlineY, zleft);
        Vec3i t1(int(xright), scanlineY, zright);

        horizontal_line_3d(t0, t1, uvleft, uvright, image, intensity, zbuffer);

        xleft += invslope1;
        xright += invslope2;
    }
}

void fill_triangle(Vec3i p0, Vec3i p1, Vec3i p2, Vec2i uv0, Vec2i uv1, Vec2i uv2, TGAImage &image, float intensity, int zbuffer[])
{
    if (p0.y < p1.y)
        std::swap(p0, p1);
    if (p0.y < p2.y)
        std::swap(p0, p2);
    if (p1.y < p2.y)
        std::swap(p1, p2);

    if (uv0.y < uv1.y)
        std::swap(uv0, uv1);
    if (uv0.y < uv2.y)
        std::swap(uv0, uv2);
    if (uv1.y < uv2.y)
        std::swap(uv1, uv2);

    int x3 = getX(Vec2i(p0.x, p0.y), Vec2i(p2.x, p2.y), p1.y);
    int xuv3 = getX(uv0, uv2, uv1.y);
    Vec2i uv3(xuv3, uv1.y);

    int z3 = getZ(p0, p2, Vec2i(int(x3), p1.y));

    Vec3i p3(x3, p1.y, z3);
    fill_bottom_flat_triangle(p0, p1, p3, uv0, uv1, uv3, image, intensity, zbuffer);
    fill_top_flat_triangle(p2, p1, p3, uv2, uv1, uv3, image, intensity, zbuffer);
}

void triangle(Vec2i p0, Vec2i p1, Vec2i p2, TGAImage &image, TGAColor col)
{
    if (p0.y > p1.y)
        std::swap(p0, p1);
    if (p0.y > p2.y)
        std::swap(p0, p2);
    if (p1.y > p2.y)
        std::swap(p1, p2);

    line(p0, p1, image, col);
    line(p1, p2, image, col);
    line(p0, p2, image, col);
}

int main(int argc, char** argv)
{
    if (argc == 2)
        model = new Model(argv[1]);
    else
        model = new Model("obj/african_head.obj");

    srand(time(NULL));

    TGAImage image(width, height, TGAImage::RGB);
    Vec3f light_dir(0, 0, -1);

    int *zbuffer = new int[width * height];
    for (int i = 0; i < width * height; i++)
        zbuffer[i] = std::numeric_limits<int>::min();

    for (int i = 0; i < model->nfaces(); i++)
    {
        std::vector<int> face = model->face(i);

        Vec3f world_coords[3];
        Vec3i screen_coords[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f v = model->vert(face[j]);

            int x = (v.x+1.)*width/2.;
            int y = (v.y+1.)*height/2.;

            world_coords[j] = v;
            screen_coords[j] = Vec3i(x, y, v.z);
        }

        Vec3f n = (world_coords[2] - world_coords[0]) ^ (world_coords[1] - world_coords[0]); // ^ is redefined for cross product
        n.normalize();

        float intensity = n * light_dir; // * is redefined for dot product

        if (intensity > 0) // back-face culling
        {
            Vec2i uv[3];
            for (int j = 0; j < 3; j++)
                uv[j] = model->uv(i, j);
            std::cout << "Vertex " << world_coords[0].x << " " << world_coords[0].y << " " << world_coords[0].z << std::endl;
            std::cout << "Screen coordinates " << screen_coords[0].x << " " << screen_coords[0].y << std::endl;
            std::cout << "Texture " << uv[0].x << " " << uv[0].y << std::endl << std::endl;

            fill_triangle(
                screen_coords[0],
                screen_coords[1],
                screen_coords[2],
                uv[0],
                uv[1],
                uv[2],
                image,
                intensity,
                zbuffer
            );
            /*
            triangle(
                Vec2i(screen_coords[0].x, screen_coords[0].y),
                Vec2i(screen_coords[1].x, screen_coords[1].y),
                Vec2i(screen_coords[2].x, screen_coords[2].y),
                image,
                red
            );
            */
        }
    }

    image.flip_vertically(); // i want to have the origin at the left bottom corner of the image
    image.write_tga_file("output.tga");

    delete[] zbuffer;
    delete model;

    return 0;
}

